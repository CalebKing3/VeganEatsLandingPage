# Vegan Eats Landing Page

A landing page to capture customer interest and highlight what our project will be in the next coming months.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

This projects uses
* NPM
* Gulp

### Installing

A step by step series of examples that tell you have to get a development env running

After Installation run

```
npm install
```

And to see the project work locally run

```
gulp dev
```

### Advanced Usage

After installation, run `npm install` and then run `gulp dev` which will open up a preview of the template in your default browser, watch for changes to core template files, and live reload the browser when changes are saved. You can view the `gulpfile.js` to see which tasks are included with the dev environment.

#### Gulp Tasks

- `gulp` the default task that builds everything
- `gulp dev` browserSync opens the project in your default browser and live reloads when changes are made
- `gulp sass` compiles SCSS files into CSS
- `gulp minify-css` minifies the compiled CSS file
- `gulp minify-js` minifies the themes JS file
- `gulp copy` copies dependencies from node_modules to the vendor directory

## Deployment

This project is served on [Gitlab Pages](https://about.gitlab.com/features/pages/)
- Using the [Git Feature Workflow ](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow)
- To deploy code to this project complete the following

* Create a branch off of `origin/master`
* Complete code changes and push to your branch
* Submit a merge request in gitlab
> Your build must pass before it can be merged into master


## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository]().

## Authors

* **Caleb King** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Jordan Rashad